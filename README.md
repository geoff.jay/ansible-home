# Domain Network Setup

[WIP] This entire thing only exists to help me manage my network and has issues.

Ansible Playbook to configure servers on my personal network. This will probably
not be of any use to anyone else.

## Dependencies

### Galaxy Roles

These need to be installed with `ansible-galaxy install <role>`

* git+https://gitlab.com/crdc/apex/ansible-roles/ansible-timescaledb

## Setup

```sh
ansible-playbook -i inventory/domain -u cap -t setup -K playbook.yml
ansible-playbook -i inventory/consul -u cap -t bootstrap -K playbooks/consul.yml
ansible-playbook -i inventory/consul -u cap -t node,consul -K playbooks/consul.yml
ansible-playbook -i inventory/plantd -u cap -t plantd -K playbooks/plantd.yml
```

### Plan

The end goal would be to run a single playbook as

```sh
ansible-playbook -i inventory/domain -u cap -K playbook.yml
```

## Services

Some other services to consider adding:

* DNS
* minikube
* Docker
* Istio

### API

* [ ] Node
* [ ] Postgraphile
* [ ] Plantd Master

### Consul

* [x] Bootstrap
* [x] Node
* Services
  * [x] SSH
  * [ ] plantctl

### Database

* [x] PostgreSQL
* [x] TimescaleDB
* [ ] MongoDB
* [ ] Redis

### Plex

* [x] plexmediaserver

Not sure how to configure this through `ansible` yet. There's also an initial
registration step that's manual, and in my case needed to be initiated through
an SSH tunnel.

### Web Server

* [ ] Nginx

### FreeIPA Server

Maybe. This is more out of interest than anything else.

### File Server

NFS is the only one that's really needed, but CIFS might be a nice to have for
access from mobile/TV.

* [x] NFS
* [ ] SMB/CIFS
* [ ] GlusterFS
